from dearpygui import dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI

from injections import freeze_total_ammo
from objects import (
    credit_count,
    element_zero,
    fuel,
    hp,
    iridium,
    lp,
    medigel,
    palladium,
    paragon,
    platinum,
    probes,
    renegade,
    used_ammo_in_clip,
)


@simple_trainerbase_menu("Mass Effect 2", 730, 300)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                GameObjectUI(hp, "HP", "F1"),
                GameObjectUI(used_ammo_in_clip, "Used Ammo In Clip", "F2"),
                CodeInjectionUI(freeze_total_ammo, "Feeze Total Ammo", "F3"),
                SeparatorUI(),
                GameObjectUI(lp, "LP"),
                GameObjectUI(paragon, "Paragon"),
                GameObjectUI(renegade, "Renegade"),
            )

        with dpg.tab(label="Resources"):
            add_components(
                GameObjectUI(credit_count, "Credits"),
                GameObjectUI(medigel, "Medi-gel"),
                GameObjectUI(element_zero, "Element Zero"),
                GameObjectUI(iridium, "Iridium"),
                GameObjectUI(palladium, "Palladium"),
                GameObjectUI(platinum, "Platinum"),
                GameObjectUI(probes, "Probes"),
                GameObjectUI(fuel, "Fuel"),
            )

        with dpg.tab(label="Misc"):
            add_components(SpeedHackUI())
