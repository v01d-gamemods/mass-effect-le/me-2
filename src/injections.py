from trainerbase.codeinjection import CodeInjection
from trainerbase.memory import pm


freeze_total_ammo = CodeInjection(
    pm.base_address + 0xBFA79,
    b"\x90\x90",
)
