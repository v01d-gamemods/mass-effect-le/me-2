from trainerbase.gameobject import GameFloat, GameInt
from trainerbase.memory import Address, pm

from memory import fame_base_address, lp_address, used_ammo_in_clip_address


paragon = GameInt(fame_base_address.inherit(extra_offsets=[0x8]))
renegade = GameInt(fame_base_address.inherit(extra_offsets=[0xC]))

base_address = Address(pm.base_address + 0x17625F0, [0x0, 0x1B0, 0xA24, 0x3BC])

resource_base_address = base_address.inherit(extra_offsets=[0x478])
credit_count = GameInt(resource_base_address.inherit(extra_offsets=[0x304]))
medigel = GameInt(resource_base_address.inherit(extra_offsets=[0x308]))
element_zero = GameInt(resource_base_address.inherit(extra_offsets=[0x30C]))
iridium = GameInt(resource_base_address.inherit(extra_offsets=[0x310]))
palladium = GameInt(resource_base_address.inherit(extra_offsets=[0x314]))
platinum = GameInt(resource_base_address.inherit(extra_offsets=[0x318]))
probes = GameInt(resource_base_address.inherit(extra_offsets=[0x31C]))
fuel = GameFloat(resource_base_address.inherit(extra_offsets=[0x320]))

hp = GameFloat(base_address.inherit(extra_offsets=[0x70, 0x10, 0x118]))

lp = GameInt(lp_address)

used_ammo_in_clip = GameFloat(used_ammo_in_clip_address)

# TODO: What is it?
# resource_base_address with offsets [0x324], [0x324], [0x328], [0x32C], [0x330], [0x334]?
